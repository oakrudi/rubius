<?php

namespace Rubius\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * ContentType
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rubius\AdminBundle\Entity\Repository\ContentTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContentType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     *
     * @ORM\Column(name="smallImage", type="string", length=255, nullable=true)
     */
    private $smallImage;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $smallImageObject;

    /**
     *
     * @ORM\Column(name="mediumImage", type="string", length=255, nullable=true)
     */
    private $mediumImage;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $mediumImageObject;

    /**
     *
     * @ORM\Column(name="bigImage", type="string", length=255, nullable=true)
     */
    private $bigImage;

    /**
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $bigImageObject;

    /**
     * @ORM\OneToMany(targetEntity="Rubius\AdminBundle\Entity\Content", mappedBy="contentType")
     */
    protected $contents;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ContentType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ContentType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ContentType
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set smallImage
     *
     * @param string $smallImage
     *
     * @return ContentType
     */
    public function setSmallImage($smallImage)
    {
        $this->smallImage = $smallImage;

        return $this;
    }

    /**
     * Get smallImage
     *
     * @return string
     */
    public function getSmallImage()
    {
        return $this->smallImage;
    }

    /**
     * Get smallImage URL
     *
     * @return string
     */
    public function getSmallImageUrl()
    {
        return sprintf("%s/%s",$this->getUploadDir(), $this->getSmallImage());
    }

    /**
     * Set mediumImage
     *
     * @param string $mediumImage
     *
     * @return ContentType
     */
    public function setMediumImage($mediumImage)
    {
        $this->mediumImage = $mediumImage;

        return $this;
    }

    /**
     * Get mediumImage URL
     *
     * @return string
     */
    public function getMediumImageUrl()
    {
        return sprintf("%s/%s",$this->getUploadDir(), $this->getMediumImage());
    }

    /**
     * Get mediumImage
     *
     * @return string
     */
    public function getMediumImage()
    {
        return $this->mediumImage;
    }

    /**
     * Set bigImage
     *
     * @param string $bigImage
     *
     * @return ContentType
     */
    public function setBigImage($bigImage)
    {
        $this->bigImage = $bigImage;

        return $this;
    }

    /**
     * Get bigImage
     *
     * @return string
     */
    public function getBigImage()
    {
        return $this->bigImage;
    }

    /**
     * Get bigImage URL
     *
     * @return string
     */
    public function getBigImageUrl()
    {
        return sprintf("%s/%s",$this->getUploadDir(), $this->getBigImage());
    }

    /**
     * @return mixed
     */
    public function getSmallImageObject()
    {
        return $this->smallImageObject;
    }

    /**
     * @param mixed $smallImageObject
     */
    public function setSmallImageObject($smallImageObject)
    {
        $this->smallImageObject = $smallImageObject;
    }

    /**
     * @return mixed
     */
    public function getMediumImageObject()
    {
        return $this->mediumImageObject;
    }

    /**
     * @param mixed $mediumImageObject
     */
    public function setMediumImageObject($mediumImageObject)
    {
        $this->mediumImageObject = $mediumImageObject;
    }

    /**
     * @return mixed
     */
    public function getBigImageObject()
    {
        return $this->bigImageObject;
    }

    /**
     * @param mixed $bigImageObject
     */
    public function setBigImageObject($bigImageObject)
    {
        $this->bigImageObject = $bigImageObject;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadPrePersist()
    {
        if (null !== $this->smallImageObject) {
            //remove old image id exists
            if (null !== $this->smallImage) {
                unlink($this->getUploadRootDir().'/'.$this->getSmallImage());
            }
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->smallImage = $filename . '.' . $this->smallImageObject->guessExtension();
            //move image to app directory
            $this->smallImageObject->move(
                $this->getUploadRootDir(),
                $this->smallImage
            );
            $this->smallImageObject = null;
        }

        if (null !== $this->mediumImageObject) {
            //remove old image id exists
            if (null !== $this->mediumImage) {
                unlink($this->getUploadRootDir().'/'.$this->getMediumImage());
            }
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->mediumImage = $filename . '.' . $this->mediumImageObject->guessExtension();
            //move image to app directory
            $this->mediumImageObject->move(
                $this->getUploadRootDir(),
                $this->mediumImage
            );
            $this->mediumImageObject = null;
        }

        if (null !== $this->bigImageObject) {
            //remove old image id exists
            if (null !== $this->bigImage) {
                unlink($this->getUploadRootDir().'/'.$this->getBigImage());
            }
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->bigImage = $filename . '.' . $this->bigImageObject->guessExtension();
            //move image to app directory
            $this->bigImageObject->move(
                $this->getUploadRootDir(),
                $this->bigImage
            );
            $this->bigImageObject = null;
        }

    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemove()
    {
        if (null !== $this->smallImage) {
            unlink($this->getUploadRootDir().'/'.$this->getSmallImage());
        }

        if (null !== $this->mediumImage) {
            unlink($this->getUploadRootDir().'/'.$this->getMediumImage());
        }

        if (null !== $this->bigImage) {
            unlink($this->getUploadRootDir().'/'.$this->getBigImage());
        }


    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/contentTypes' ;
    }
}
