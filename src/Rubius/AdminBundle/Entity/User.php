<?php

namespace Rubius\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rubius\AdminBundle\Entity\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable, EquatableInterface, EncoderAwareInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="avatar", type="string", length=255, options={"default"="http://www.gravatar.com/avatar/?d=identicon"})
     */
    private $avatar;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", cascade={"PERSIST"})
     */
    private $roles;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated;

    /**
     * @ORM\OneToMany(targetEntity="Rubius\AdminBundle\Entity\Content", mappedBy="createdBy")
     */
    private $createdContents;

    /**
     * @ORM\OneToMany(targetEntity="Rubius\AdminBundle\Entity\Content", mappedBy="updatedBy")
     */
    private $updatedContents;

    public function __construct()
    {
        $this->isActive = true;
        $this->createdAt = new \Datetime();
        $this->updated = new \Datetime();
        $this->setAvatar('https://randomuser.me/api/portraits/lego/5.jpg');
// may not be needed, see section on salt below
// $this->salt = md5(uniqid(null, true));
    }
    
    public function getEncoderName()
    {
        return 'rubius_encoder'; // use the default encoder
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
// you *may* need a real salt depending on your encoder
// see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        if ($this->roles) {
            return $this->roles->toArray();    
        } 
        return [];
        
    }

    public function eraseCredentials()
    {
    }


    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive
// see section on salt below
// $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive
// see section on salt below
// $this->salt
            ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        if(!is_null($password)){
            $this->password = password_hash($password, PASSWORD_BCRYPT);
        }
        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add roles
     *
     * @param $roles
     * @return User
     */
    public function addRole( $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Rubius\AdminBundle\Entity\Role $role

     */
    public function removeRole($role)
    {
        $this->roles->removeElement($role);
    }

    public function isEqualTo(UserInterface $user)
    {
        return $this->id === $user->getId();
    }

    /**
     * Get Bcrypt cost to encode password
     * @return int
     */
    public function getBCryptCost()
    {
        return 14;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return User
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Add createdContent
     *
     * @param \Rubius\AdminBundle\Entity\Content $createdContent
     *
     * @return User
     */
    public function addCreatedContent(\Rubius\AdminBundle\Entity\Content $createdContent)
    {
        $this->createdContents[] = $createdContent;

        return $this;
    }

    /**
     * Remove createdContent
     *
     * @param \Rubius\AdminBundle\Entity\Content $createdContent
     */
    public function removeCreatedContent(\Rubius\AdminBundle\Entity\Content $createdContent)
    {
        $this->createdContents->removeElement($createdContent);
    }

    /**
     * Get createdContents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreatedContents()
    {
        return $this->createdContents;
    }

    /**
     * Add updatedContent
     *
     * @param \Rubius\AdminBundle\Entity\Content $updatedContent
     *
     * @return User
     */
    public function addUpdatedContent(\Rubius\AdminBundle\Entity\Content $updatedContent)
    {
        $this->updatedContents[] = $updatedContent;

        return $this;
    }

    /**
     * Remove updatedContent
     *
     * @param \Rubius\AdminBundle\Entity\Content $updatedContent
     */
    public function removeUpdatedContent(\Rubius\AdminBundle\Entity\Content $updatedContent)
    {
        $this->updatedContents->removeElement($updatedContent);
    }

    /**
     * Get updatedContents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUpdatedContents()
    {
        return $this->updatedContents;
    }
}
