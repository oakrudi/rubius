<?php

namespace Rubius\AdminBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Rubius\DataTablesBundle\Library\ColumnObject;

class UserRepository extends EntityRepository {

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getGridQuery($columns)
    {
        $qb = $this->createQueryBuilder('users');
        $selectColumns = [];
        /** @var ColumnObject $col */
        foreach ($columns as $col) {
            if (!is_null($col->getTableAlias())) {
                $selectColumns[] = sprintf(
                    "%s.%s as %s",
                    $col->getTableAlias(),
                    ($col->getDbField()? $col->getDbField(): $col->getColumnAlias()),
                    $col->getColumnAlias()
                );
            }
        }
        $qb
            ->select($selectColumns)
        ->leftJoin('users.roles', 'roles')
        ->groupBy('users.id');
//var_dump($qb->getQuery()->getSQL()); die;
        return $qb;

    }
}