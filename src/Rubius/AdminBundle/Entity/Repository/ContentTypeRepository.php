<?php
/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use Rubius\DataTablesBundle\Library\ColumnObject;

class ContentTypeRepository extends EntityRepository
{

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDataTableQuery($columns)
    {
        $qb = $this->createQueryBuilder('contentTypes');
        $selectColumns = [];
        /** @var ColumnObject $col */
        foreach ($columns as $col) {
            if (!is_null($col->getTableAlias())) {
                $selectColumns[] = sprintf(
                    "%s.%s as %s",
                    $col->getTableAlias(),
                    ($col->getDbField()? $col->getDbField(): $col->getColumnAlias()),
                    $col->getColumnAlias()
                );
            }
        }
        $qb
            ->select($selectColumns);
//var_dump($qb->getQuery()->getSQL()); die;
        return $qb;

    }
}