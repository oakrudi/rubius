<?php
/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\Form\CMS;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentTypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title', 'text',
                [
                    'attr' => [
                        'class' => 'form-control title-input'
                    ]
                ]
            )
            ->add(
                'description', 'text',
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control description-input'
                    ]
                ]
            )
            ->add(
                'url', 'text',
                [
                    'attr' => [
                        'class' => 'form-control url-input'
                    ]
                ]
            )
            ->add(
                'smallImageObject', 'file',
                [
                    'required' => false,
                    'data_class' => null,
                    'attr' => [

                        'class' => 'form-control smallImage-input'
                    ]
                ]
            )
            ->add(
                'mediumImageObject', 'file',
                [
                    'required' => false,
                    'data_class' => null,
                    'attr' => [
                        'class' => 'form-control mediumImage-input'
                    ]
                ]
            )
            ->add(
                'bigImageObject', 'file',
                [
                    'required' => false,
                    'data_class' => null,
                    'attr' => [
                        'class' => 'form-control bigImage-input'
                    ]
                ]
            )
        ;
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => \Rubius\AdminBundle\Entity\ContentType::class

            ]
        );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'content_type';
    }
}