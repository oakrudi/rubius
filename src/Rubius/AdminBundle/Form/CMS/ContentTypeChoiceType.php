<?php
/**
 * Created by PhpStorm.
 * User: rudi
 * Date: 18-10-2015
 * Time: 0:55
 */

namespace Rubius\AdminBundle\Form\CMS;


use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentTypeChoiceType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'class'=> 'Rubius\AdminBundle\Entity\ContentType',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('ct');
                },
                'empty_data' => null,
                'choice_label' => 'title',
                'choice_value' => 'id',
                'attr' => [
                    'class' => 'form-control'
                ]
            ]
        );
    }

    public function getParent()
    {
        return 'entity';
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'content_type_choice';
    }
}