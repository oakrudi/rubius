<?php
/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\Form\CMS;
use Rubius\AdminBundle\Entity\Content;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title', 'text' ,
            [
                'label' => 'rubiusAdmin.cms.content.create.titleField',
                'attr' => [
                    'class' => 'form-control title-input',
                    'placeholder' => 'rubiusAdmin.cms.content.create.titleFieldPlaceholder'
                ]
            ]
        )->add(
            'alias', 'text' ,
            [
                'label' => 'rubiusAdmin.cms.content.create.aliasField',
                'attr' => [
                    'placeholder' => 'rubiusAdmin.cms.content.create.titleFieldPlaceholder',
                    'class' => 'form-control alias-input'
                ]
            ]
        )
            ->add(
                'intro', 'textarea' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.introField',
                    'attr' => [
                        'class' => 'form-control intro-input ckeditor'
                    ]
                ]
            )
            ->add(
                'body', 'textarea' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.bodyField',
                    'attr' => [
                        'class' => 'form-control body-input ckeditor'
                    ]
                ]
            )
            ->add(
                'active', 'checkbox' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.activeField',
                    'attr' => [
                        'class' => 'checkbox-input'
                    ]
                ]
            )->add(
                'publishedDate', 'date' ,
                [
                    'label' => 'rubiusAdmin.cms.content.create.publishedDateField',
                    'attr' => [
                        'class' => 'form-control publishedDate-input',
                        'data-inputmask'=>"'alias': 'dd/mm/yyyy'",
                        'data-mask'=> null
                    ]
                ]
            )
            ->add(
                'smallImageObject', 'file' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.smallImageField',
                    'attr' => [
                        'class' => 'form-control smallImage-input'
                    ]
                ]
            )
            ->add(
                'mediumImageObject', 'file' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.mediumImageField',
                    'attr' => [
                        'class' => 'form-control mediumImage-input'
                    ]
                ]
            )
            ->add(
                'bigImageObject', 'file' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.bigImageField',
                    'attr' => [
                        'class' => 'form-control bigImage-input'
                    ]
                ]
            )
            ->add(
                'metaTags', 'text' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.metaTagsField',
                    'attr' => [
                        'class' => 'form-control metaTags-input'
                    ]
                ]
            )
            ->add(
                'metaDescription', 'text' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.metaDescriptionField',
                    'attr' => [
                        'class' => 'form-control metaDescription-input'
                    ]
                ]
            )
            ->add(
                'forwardUrl', 'text' ,
                [
                    'required' => false,
                    'label' => 'rubiusAdmin.cms.content.create.forwardUrlField',
                    'attr' => [
                        'placeholder' => 'rubiusAdmin.cms.content.create.forwardUrlFieldPlaceholder',
                        'class' => 'form-control forwardUrl-input'
                    ]
                ]
            )
            ->add('contentType', 'content_type_choice')
        ;
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => false,
            'class' => Content::class,
            'data_class' => Content::class,
            'translation_domain' => 'rubiusAdmin'
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'content';
    }
}