<?php
/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleType extends AbstractType
{
    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'multiple' => true,
                'expanded' => true,
                'class' => 'Rubius\AdminBundle\Entity\Role',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r');
                },
                'choice_label'=> 'name',
                'choice_value' => 'role',
                'choice_attr' => function () {
                    return array('class' => 'minimal role-input');
                },
                'label' => 'rubiusAdmin.users.create.rolesField',
                'attr' => ['id' => 'roles_choice']
            ]
        );
    }


    public function getParent()
    {
        return 'entity';
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'role_select';
    }
}