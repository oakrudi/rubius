<?php

/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */
namespace Rubius\AdminBundle\Controller\CMS;

use Rubius\AdminBundle\Entity\Content;
use Rubius\DataTablesBundle\Library\DataTablesInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContentController
 * @package Rubius\AdminBundle\Controller\CMS
 * @Security("has_role('ROLE_ADMIN')")
 */
class ContentController extends \Rubius\AdminBundle\Controller\DefaultController
{

    /**
     * @Route(path="/", name="rubius_content_index")
     */
    public function indexAction()
    {
        /** @var DataTablesInterface $table */
        $table = $this->get('datatable.factory')->getTable('contents');

        return $this->render(
            'RubiusAdminBundle:CMS/content:index.html.twig',
            [
                'table' => $table->getDataTableObject($this->generateUrl('rubius_content_datatable'))
            ]
        );

    }

    /**
     * @Route(path="/getContentTypesData", name="rubius_content_datatable")
     */
    public function getContentDataAction()
    {
        /** @var DataTablesInterface $table */
        $table = $this->get('datatable.factory')->getTable('contents');

        return new JsonResponse(
            $table->getData()
        );
    }

    /**
     * @Route(path="/create", name="rubius_content_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm('content');

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            /** @var Content $content */
            $content = $form->getData();
            $this->get('doctrine.orm.default_entity_manager')->persist($content);
            $this->get('doctrine.orm.default_entity_manager')->flush();

            return $this->redirect($this->generateUrl('rubius_content_index'));
        }

        return $this->render(
            '@RubiusAdmin/CMS/content/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );


    }

    /**
     * @Route(path="/{id}/edit", name="rubius_content_edit")
     * @param Request $request
     * @param Content $content
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Content $content)
    {
        $form = $this->createForm('content', $content);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $content->uploadPrePersist();
            $this->get('doctrine.orm.default_entity_manager')->flush($content);

            return $this->redirect($this->generateUrl('rubius_content_index'));
        }
        return $this->render(
            '@RubiusAdmin/CMS/content/create.html.twig',
            [
                'form' => $form->createView(),
                'content' => $content
            ]
        );
    }

    /**
     * @Route(path="/{id}/delete", name="rubius_content_delete")
     */
    public function deleteAction()
    {

    }
}