<?php
/**
 * Author: Rudi
 */

namespace Rubius\AdminBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RubiusController
 * @package Rubius\AdminBundle\Controller
 * @Security("has_role('ROLE_USER')")
 */
class RubiusController extends DefaultController {

    /**
     * @Route(path="/", name="rubius_index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        return $this->render('RubiusAdminBundle:Rubius:index.html.twig');
    }
}