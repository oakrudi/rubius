<?php

namespace Rubius\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package Rubius\AdminBundle\Controller
 * @Security("has_role('ROLE_USER')")
 */
class DefaultController extends Controller
{

}
