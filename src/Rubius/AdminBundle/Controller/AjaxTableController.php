<?php
/**
 * Author: Rudi
 */

namespace Rubius\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxTableController
 * @package Rubius\AdminBundle\Controller
 * @Security("has_role('ROLE_USER'")
 */
class AjaxTableController extends DefaultController{

    /**
     * @Route(path="/{alias}", name="rubius_admin_get_datatables_data")
     */
    public function getTableDataAction($alias)
    {
        $table = $this->get('dataTables.factory')->getTable($alias);
        return new JsonResponse($table->getData());
    }

}