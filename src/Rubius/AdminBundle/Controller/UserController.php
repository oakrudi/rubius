<?php
/**
 * Author: Rudi
 */

namespace Rubius\AdminBundle\Controller;

use Rubius\AdminBundle\Entity\User;
use Rubius\AdminBundle\RubiusAdminBundle;
use Rubius\DataTablesBundle\Library\DataTablesInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class UserController
 * @package Rubius\AdminBundle\Controller
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends DefaultController {

    /**
     * @Route(path="/", name="rubius_users")
     */
    public function indexAction()
    {
        /** @var DataTablesInterface $table */
        $table = $this->get('datatable.factory')->getTable('users');

        return $this->render(
            'RubiusAdminBundle:User:index.html.twig',
            [
                'table' => $table->getDataTableObject($this->generateUrl('rubius_users_datatable'))
            ]
        );
    }

    /**
     * @Route(path="/datatable-data", name="rubius_users_datatable")
     */
    public function getUsersAction()
    {
        /** @var DataTablesInterface $table */
        $table = $this->get('datatable.factory')->getTable('users');

        return new JsonResponse(
            $table->getData()
        );
    }

    /**
     * @Route(path="/create", name="rubius_users_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createUserAction(Request $request)
    {
        $form = $this->createForm('user_create', new User());
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {

            /** @var User $user */
            $user = $form->getData();
            //$user->setPassword(password_hash($user->getPassword(), PASSWORD_BCRYPT, array('cost' => 13)));
            $this->get('doctrine.orm.default_entity_manager')->persist($user);
            $this->get('doctrine.orm.default_entity_manager')->flush();
            return $this->redirect($this->generateUrl('rubius_users'));
        }

        return $this->render('@RubiusAdmin/User/create.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route(path="/{id}/edit", name="rubius_users_edit")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function EditUserAction(User $user, Request $request)
    {
        $form = $this->createForm('user_create', $user);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            
            /** @var User $user */
            $user = $form->getData();

            $this->get('doctrine.orm.default_entity_manager')->persist($user);
            $this->get('doctrine.orm.default_entity_manager')->flush();
            return $this->redirect($this->generateUrl('rubius_users'));
        }

        return $this->render('@RubiusAdmin/User/create.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route(path="/{id}/delete", name="rubius_users_delete")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function deleteAction(User $user)
    {
        if (is_null($user)) {
            throw new \Exception("Invalid Used");
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->remove($user);
        $em->flush();
        return $this->redirect($this->generateUrl('rubius_users'));

    }

}