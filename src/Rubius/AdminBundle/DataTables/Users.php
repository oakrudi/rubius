<?php
/**
 * Author: Rudi
 */

namespace Rubius\AdminBundle\DataTables;


use Doctrine\ORM\QueryBuilder;
use Rubius\DataTablesBundle\Library\ColumnObject;
use Rubius\DataTablesBundle\Library\DataTablesInterface;
use Rubius\DataTablesBundle\Strategy\DataTablesStrategy;

class Users extends DataTablesStrategy implements DataTablesInterface {


    /**
     * Build your column structure defining $this->setColumns($columnsArray)
     */
    public function defineColumns()
    {
        $columns = [];

        $column = new ColumnObject('rubiusAdmin.users.index.gridId', 'id', 'users', 'rubiusAdmin');
        $columns[] = $column;

        $column = new ColumnObject('rubiusAdmin.users.index.gridUsername', 'username', 'users', 'rubiusAdmin');
        $columns[] = $column;

        $column = new ColumnObject('rubiusAdmin.users.index.gridStatus', 'isActive', 'users', 'rubiusAdmin');
        $columns[] = $column;

        $column = new ColumnObject('rubiusAdmin.users.index.gridEmail', 'email', 'users', 'rubiusAdmin');
        $columns[] = $column;

        $column = new ColumnObject('rubiusAdmin.users.index.gridRole', 'roleName', 'roles', 'rubiusAdmin');
        $columns[] = $column->setDbField('name');

        $column = new ColumnObject('rubiusAdmin.users.index.gridActions', 'actions', null, 'rubiusAdmin');
        $columns[] = $column->setSortable(false);

        $this->setColumns($columns);
    }

    protected function getFormattedData($rows)
    {
        $data = [];
        foreach ($rows as $row) {
            $dataRow = $this->mapAutomaticFields($row);
            $dataRow['isActive'] = $this->getStatus($row);
            $dataRow['actions'] = $this->getActions($row);

            $data[] = $dataRow;
        }

        return $data;
    }


    /**
     *
     */
    public function setQueryBuilderObject()
    {
        return $this->setQueryBuilder($this->getEntityManager()->getRepository('RubiusAdminBundle:User')->getGridQuery($this->getColumns()));
    }

    /**
     * @return mixed
     */
    public function setWhereStatement()
    {
        if ($this->getRequest()->has('sSearch')){
            $this->getQueryBuilder()->andWhere(
                $this->getQueryBuilder()
                    ->expr()->like('users.email',sprintf("'%%%s%%'", $this->getRequest()->get('sSearch')))
            );
        }

    }

    /**
     * @param $row
     * @return string
     */
    private function getStatus($row)
    {
        return $this->getRenderer()->render(
            '@RubiusAdmin/User/partials/user-status-flag.html.twig',
            [
                'row' => $row
            ]
        );
    }

    /**
     * @param $row
     * @return string
     */
    private function getActions($row)
    {
        return $this->getRenderer()->render(
            '@RubiusAdmin/User/partials/index-grid-actions.html.twig',
            [
                'row' => $row
            ]
        );
    }


}