<?php
/**
 * @Author: Rúdi Rocha <rudi.rocha@gmail.com>
 */

namespace Rubius\AdminBundle\DataTables;


use Doctrine\ORM\QueryBuilder;
use Rubius\DataTablesBundle\Library\ColumnObject;
use Rubius\DataTablesBundle\Library\DataTablesInterface;
use Rubius\DataTablesBundle\Strategy\DataTablesStrategy;

class Contents extends DataTablesStrategy implements DataTablesInterface
{

    /**
     * @return QueryBuilder
     */
    public function setQueryBuilderObject()
    {
        return $this->setQueryBuilder(
            $this->getEntityManager()
                ->getRepository('RubiusAdminBundle:Content')
                ->getDataTableQuery($this->getColumns())
        );
    }

    /**
     * Build your column structure defining $this->setColumns($columnsArray)
     */
    public function defineColumns()
    {
        $columns = [];

        $column = new ColumnObject('rubiusAdmin.cms.content.index.gridId', 'id', 'contents','rubiusAdmin');
        $columns[] = $column;
        $column = new ColumnObject('rubiusAdmin.cms.content.index.gridTitle', 'title', 'contents','rubiusAdmin');
        $columns[] = $column;
        $column = new ColumnObject('rubiusAdmin.cms.content.index.gridAlias', 'alias', 'contents','rubiusAdmin');
        $columns[] = $column;
        $column = new ColumnObject('rubiusAdmin.cms.content.index.gridActive', 'active', 'contents','rubiusAdmin');
        $columns[] = $column;
        $column = new ColumnObject('rubiusAdmin.cms.content.index.gridPublishedDate', 'publishedDate', 'contents','rubiusAdmin');
        $columns[] = $column;
        $column = new ColumnObject('rubiusAdmin.cms.content.index.gridUpdatedAt', 'updatedAt', 'contents','rubiusAdmin');
        $columns[] = $column;
        $column = new ColumnObject('rubiusAdmin.cms.content.index.gridActions', 'actions', null, 'rubiusAdmin');
        $columns[]= $column->setSortable(false);

        $this->setColumns($columns);
    }

    /**
     * Set Where statement to QueryBuilder
     */
    public function setWhereStatement()
    {
        if ($this->getRequest()->has('sSearch') and $this->getRequest()->get('sSearch') != ''){
            $this->getQueryBuilder()->andWhere(
                $this->getQueryBuilder()
                    ->expr()->like('contents.title',sprintf("'%%%s%%'", $this->getRequest()->get('sSearch')))
            );
        }
    }

    protected function getFormattedData($rows)
    {
        $data = [];
        foreach ($rows as $row) {
            $dataRow = $this->mapAutomaticFields($row);
            $dataRow['publishedDate'] = $row['publishedDate']->format('d-m-Y');
            $dataRow['updatedAt'] = $row['updatedAt']->format('d-m-Y');
            $dataRow['actions'] = $this->getActions($row);
            $data[] = $dataRow;
        }

        return $data;
    }

    private function getActions($row)
    {
        return $this->getRenderer()->render(
            '@RubiusAdmin/CMS/content-type/partials/content-dt-actions.html.twig',
            [
                'row' => $row
            ]
        );
    }
}