//Gruntfile
module.exports = function(grunt) {

    //Initializing the configuration object
    grunt.initConfig({
        copy: {
            main:{
                files:[
                    //AdminLte Files
                    {
                        src: './bower_components/AdminLTE/dist/css/AdminLTE.min.css',
                        dest: './web/assets/rubius/css/AdminLTE.min.css'
                    },
                    {
                        cwd: './bower_components/AdminLTE/dist/css/skins/',
                        src: '**/*',
                        dest: './web/assets/rubius/css/skins',
                        expand: true
                    },
                    {
                        src: './bower_components/AdminLTE/dist/js/app.min.js',
                        dest: './web/assets/rubius/js/app.min.js'
                    },

                    //Bootstrap Files
                    {
                        src: ['./bower_components/bootstrap/dist/js/bootstrap.min.js'],
                        dest: './web/assets/bootstrap/js/bootstrap.min.js'
                    },
                    {
                        src: ['./bower_components/bootstrap/dist/css/bootstrap.min.css'],
                        dest: './web/assets/bootstrap/css/bootstrap.min.css'
                    },
                    //font awesome
                    {
                        src:'./bower_components/font-awesome/css/font-awesome.min.css',
                        dest: './web/assets/rubius/css/font-awesome.min.css'
                    },
                    {
                        cwd: './bower_components/font-awesome/fonts/',
                        src: '**/*',
                        dest: './web/assets/rubius/fonts',
                        expand: true
                    },
                    //ckEditor
                    {
                        src:'./bower_components/ckeditor/ckeditor.js',
                        dest: './web/assets/ckeditor/ckeditor.js'
                    },
                    {
                        src:'./src/Rubius/AdminBundle/Resources/assets/ckeditor/config.js',
                        dest: './web/assets/ckeditor/config.js'
                    },
                    {
                        src:'./src/Rubius/AdminBundle/Resources/assets/ckeditor/styles.js',
                        dest: './web/assets/ckeditor/styles.js'
                    },
                    {
                        cwd: './bower_components/ckeditor/skins/',
                        src: '**/*',
                        dest: './web/assets/ckeditor/skins',
                        expand: true
                    },
                    {
                        cwd: './bower_components/ckeditor/plugins/',
                        src: '**/*',
                        dest: './web/assets/ckeditor/plugins',
                        expand: true
                    },
                    {
                        cwd: './bower_components/ckeditor/lang/',
                        src: '**/*',
                        dest: './web/assets/ckeditor/lang',
                        expand: true
                    },
                    //Bootstrap datepicker
                    {
                        src:'./bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        dest: './web/assets/bootstrap-datepicker/bootstrap-datepicker.js'
                    },
                    {
                        src:'./bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',
                        dest: './web/assets/bootstrap-datepicker/bootstrap-datepicker.css'
                    },
                    {
                        cwd: './bower_components/bootstrap-datepicker/dist/js/locales/',
                        src: '**/*',
                        dest: './web/assets/bootstrap-datepicker/locales',
                        expand: true
                    },
                    //rubius Files
                    {
                        src: ['./src/Rubius/AdminBundle/Resources/assets/img/logo_smal_h.png'],
                        dest: './web/assets/rubius/img/logo_small_h.png'
                    },
                    {
                        src: ['./src/Rubius/AdminBundle/Resources/assets/img/logo_small_collapsed.png'],
                        dest: './web/assets/rubius/img/logo_small_collapsed.png'
                    },
                    {
                        src: ['./src/Rubius/AdminBundle/Resources/assets/img/logo_small_full.png'],
                        dest: './web/assets/rubius/img/logo_small_full.png'
                    }
                ]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', ['copy']);


};